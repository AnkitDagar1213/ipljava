import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.util.TreeMap;

public class matchesPerYear {
    public static Map<String,Integer> calculateMatchesPerYear(String matchesFile){

        Map<String, Integer> matchesPerYear = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String year = data[1];

                matchesPerYear.put(year, matchesPerYear.getOrDefault(year, 0) + 1);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return matchesPerYear;
        }
    }

    public static void main(String[] args) {
        String matchesFile = "src/main/java/data/matches.csv";

        Map<String,Integer> result=calculateMatchesPerYear(matchesFile);

//        System.out.println(result);
        for(String i: result.keySet()){
            System.out.println(i+" "+result.get(i));
        }
    }
}
