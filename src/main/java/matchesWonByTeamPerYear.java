import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class matchesWonByTeamPerYear {
    public static Map<String, Map<String, Integer>> calculateMatchesWonByTeamPerYear(String matchesFile){
        Map<String, Map<String, Integer>> matchesWonByTeamPerYear = new TreeMap<>();
        Map<String, Integer> teamWinsByYear = new TreeMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();

            while ((row = br.readLine()) != null) {

                String[] data = row.split(",");
                String year = data[1];
                String winner = data[10];

                if (winner == "") {
                    continue;
                }
                if (matchesWonByTeamPerYear.containsKey(year)) {
                    teamWinsByYear = matchesWonByTeamPerYear.get(year);
                } else {
                    matchesWonByTeamPerYear.put(year, new HashMap<>());
                }

                teamWinsByYear.put(winner, teamWinsByYear.getOrDefault(winner, 0) + 1);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            {
                return matchesWonByTeamPerYear;
            }
        }
    }
    public static void main(String[] args) {
        String matchesFile = "src/main/java/data/matches.csv";

        Map<String, Map<String, Integer>> result=calculateMatchesWonByTeamPerYear(matchesFile);

//        System.out.println(result);
        for(String i: result.keySet()){
            System.out.println(i+" "+result.get(i));
        }

    }
}
