import java.io.*;
import java.util.*;

public class topEconomicalBowler {
    public static Map<String,Double> calculateTopEconomicalBowler (String matchesFile,String deliveriesFile){
        Map<String, Double> economicalBowlers = new HashMap<>();
        ArrayList<String> teams_id = new ArrayList<>();
        Map<String, Integer> bowlerRuns = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();
        Map<String,Double> topEconomicalBowler = new HashMap<>();

        String bowlerName = "";
        double economy = Double.MAX_VALUE;

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String season = data[1];
                if (season.equals("2015")) {
                    teams_id.add(data[0]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(deliveriesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {

                String[] data = row.split(",");
                String match_Id = data[0];
                if (teams_id.contains(match_Id) && data[9].equals("0")) {
                    int balls;
                    String bowler = data[8];
                    int runs = Integer.parseInt(data[17]);
                    int extras = Integer.parseInt(data[16]);
                    int wideRuns = Integer.parseInt(data[10]);
                    int byeRuns = Integer.parseInt(data[11]);
                    int legByeRuns = Integer.parseInt(data[12]);

                    if (wideRuns == 0 && byeRuns == 0 && legByeRuns == 0) {
                        balls = 1;
                    } else {
                        balls = 0;
                    }
                    if (bowlerRuns.containsKey(bowler)) {
                        bowlerRuns.put(bowler, bowlerRuns.get(bowler) + runs + extras);
                        bowlerBalls.put(bowler, bowlerBalls.get(bowler) + balls);
                    } else {
                        bowlerRuns.put(bowler, runs + extras);
                        bowlerBalls.put(bowler, balls);
                    }
                }
            }
            for (String bowler : bowlerRuns.keySet()) {
                int runs = bowlerRuns.get(bowler);
                int balls = bowlerBalls.get(bowler);
                double economyRate = (double) runs / (balls / 6.0);
                economicalBowlers.put(bowler, economyRate);
            }
            for (Map.Entry<String, Double> i : economicalBowlers.entrySet()) {
                if (i.getValue() < economy) {
                    bowlerName = i.getKey();
                    economy = i.getValue();
                }
            }
            topEconomicalBowler.put(bowlerName,economy);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return topEconomicalBowler;
        }
    }
    public static void main(String[] args) {
        String matchesFile = "src/main/java/data/matches.csv";
        String deliveriesFile = "src/main/java/data/deliveries.csv";

        Map<String,Double> result=calculateTopEconomicalBowler(matchesFile,deliveriesFile);

//        System.out.println(result);
        for(String i: result.keySet()){
            System.out.println(i+" "+result.get(i));
        }
    }
}


