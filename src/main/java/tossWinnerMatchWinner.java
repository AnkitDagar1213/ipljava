import java.io.*;
import java.util.*;

public class tossWinnerMatchWinner {
    public static Map<String, Integer> calculateTossWinnerMatchWinner(String matchesFile){
        Map<String, Integer> tossWinnerMatchWinner = new TreeMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String tossWinner = data[6];
                String winner = data[10];
                if(tossWinner.equals(winner))
                {
                    tossWinnerMatchWinner.put(winner, tossWinnerMatchWinner.getOrDefault(winner, 0) + 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            return tossWinnerMatchWinner;
        }
    }
    public static void main(String[] args) {
        String matchesFile = "src/main/java/data/matches.csv";

        Map<String,Integer> result = calculateTossWinnerMatchWinner(matchesFile);

//            System.out.println(tossWinnerMatchWinner);
        for(String i: result.keySet()){
            System.out.println(i+" "+result.get(i));
        }

    }
}
