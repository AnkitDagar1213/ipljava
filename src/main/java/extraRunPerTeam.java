import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class extraRunPerTeam {
    public static Map<String, Integer> calculateExtraRunPerTeam(String matchesFile, String deliveriesFile) {

        Map<String, Integer> extraRunPerTeam = new TreeMap<>();
        ArrayList<String> teams_id = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(matchesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String season = data[1];
                if (season.equals("2016")) {
                    teams_id.add(data[0]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(deliveriesFile))) {

            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {

                String[] data = row.split(",");
                String match_id = data[0];

                if (teams_id.contains(match_id)) {
                    String team = data[3];
                    int runs = Integer.parseInt(data[16]);

                    extraRunPerTeam.put(team, extraRunPerTeam.getOrDefault(team, runs) + runs);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            return extraRunPerTeam;
        }

    }
    public static void main(String[] args) {
        String matchesFile = "src/main/java/data/matches.csv";
        String deliveriesFile = "src/main/java/data/deliveries.csv";

        Map<String,Integer> result=calculateExtraRunPerTeam(matchesFile,deliveriesFile);

//        System.out.println(result);
        for(String i: result.keySet()){
            System.out.println(i+" "+result.get(i));
        }
    }
}


